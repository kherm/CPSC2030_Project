$(document).ready(function() {
    addLinks();
});

function addLinks() {
    let links = document.querySelectorAll('div[class^="updateList"]')
    links.forEach(link => {
        $(link).click(function() {
            if(link.innerText != "Get Educated");
            updatePage(link.innerText);
        });
    });
}

function updatePage(link) {
    $.ajax({
        method: "GET",
        url: "local.php",
        data: {
            page: link
        },
        success: function(json) {
            $('.page').html(json);
            $('html, body').animate({
                scrollTop: $(".page").offset().top
            }, 500, "linear");
        }
    });
}