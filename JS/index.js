$(document).ready(function() {
    runLogin();
    runRegister();
    addShares();
});

function runLogin() {
    const loginBox = document.querySelector('.login');
    const loginBtn = $('#loginBtn');
    const notice = document.querySelector('.notice');
    loginBtn.click(function() {
        if ( loginBtn.html().includes("Login")) {
            $(loginBox).css('display', 'block');
        }
        else {
            logout();
        }
    });

    window.onclick = function(event) {
        if (event.target == loginBox) {
            $(loginBox).css('display', 'none');
        }
        $(notice).css('display', 'none');
    }
}

function logout() {
    $.ajax({
        method: "POST",
        url: "index.php",
        data: {
            logout: true
        },
        success: function(json) {
            $('#loginBtn').html("Login");
            $('.notice').css('display', 'block');
        }
    });
}

function runRegister() {
    $('#registerBtn').click(function() {
        $('.signin').css('display', 'none');
        $('.register').css('display', 'block');
    });
    $('#registerSubmit').click(function() {
        $('.signin').css('display', 'block');
        $('.register').css('display', 'none');
    });
}

function addShares() {
    const socialMedia = document.querySelectorAll('.share > a');
    socialMedia.forEach(icon => {
        $(icon).click(function() {
            incrementShare();
        });
    });
}

function incrementShare() {
    const articleTitle = $('article > h1').html();
    $.ajax({
        method: "POST",
        url: "article.php",
        data: {
            article: articleTitle
        },
        success: function(json) {
            let share = $('#shares > span').html();
            $('#shares > span').html(++share);
        }
    });
}