<?php

require_once 'sqlhelper.php';
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$conn = connectToDatabase();
session_start();


$type = $_GET["type"];

if ($type == "subscribe") {
    $email = $_POST["email"];
    $enterEmail = $conn->query("call addSubscriber(\"$email\")");\
    clearConnection($conn);

    $success = $twig->load('submitSuccess.html.twig');
    echo $success->render(array("email"=>$email, "type"=>$type));

} else if ($type == "storyForm") {
    $success = $twig->load('storyForm.html.twig');
    echo $success->render();

} else if ($type == "storySubmit") {
    $fName = $_POST["fName"];
    $lName = $_POST["lName"];
    $email = $_POST["email"];
    $date = $_POST["date"];
    $content = $_POST["content"];
    $enterStory = $conn->query("call addUserStory(\"$fName\", \"$lName\", \"$email\", \"$date\", \"$content\")");\
    clearConnection($conn);

    $success = $twig->load('submitSuccess.html.twig');
    echo $success->render(array("name"=>$fName, "type"=>$type));

} else if ($type == "login") {
    $uname = $_POST["uname"];
    $psw = $_POST["psw"];

    $login = $conn->query("call ckLogin(\"$uname\", \"$psw\")");
    clearConnection($conn);

    if($login->num_rows > 0) {
        $_SESSION["loggedIn"] = true;
        header('Location: ./index.php');
    } else {
        $failure = $twig->load('submitSuccess.html.twig');
        echo $failure->render(array("type"=>$type));
    }

} else if ($type == "register") {
    $uname = $_POST["uname"];
    $psw = $_POST["psw"];

    $login = $conn->query("call addUser(\"$uname\", \"$psw\")");
    clearConnection($conn);

    $_SESSION["loggedIn"] = true;
    include './index.php';
}

?>