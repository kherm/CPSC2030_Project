<?php
echo "session_start";
require_once 'sqlhelper.php';
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$conn = connectToDatabase();
session_start();


if (!isset($_SESSION["loggedIn"])) {
    $_SESSION["loggedIn"] = false;
}
if (isset($_POST["logout"])) {
    if ( $_POST["logout"] == true) {
        session_destroy();
        session_start();
        $_SESSION["loggedIn"] = false;
    }
}

if (isset($_GET["about"]) ) {
    $homePage = $twig->load('aboutPage.html.twig');
    echo $homePage->render();

} else if (isset($_GET["resources"]) ) {
    $homePage = $twig->load('resourcePage.html.twig');
    echo $homePage->render();

} else {
    $articleHighlights = $conn->query('call getNumNewestArticles(8)');
    clearConnection($conn);

    if($articleHighlights) {
        $articles = $articleHighlights->fetch_all(MYSQLI_ASSOC);
        $homePage = $twig->load('homePage.html.twig');
        $log = $_SESSION["loggedIn"];
        echo $homePage->render(array("articleHighlights"=>$articles, "login"=>"$log"));
    }
}




?>