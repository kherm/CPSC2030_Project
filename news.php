<?php

require_once 'sqlhelper.php';
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$conn = connectToDatabase();
session_start();

if ($_SESSION["loggedIn"] == true) {
    $type = $_GET["type"];

    $article = $conn->query("call getArticlesFor(\"$type\")");
    clearConnection($conn);

    if($article) {
        $articleInfo = $article->fetch_all(MYSQLI_ASSOC);
        $articlePage = $twig->load('news.html.twig');
        $log = $_SESSION["loggedIn"];
        echo $articlePage->render(array("articleHighlights"=>$articleInfo, "type"=>$type, "login"=>"$log"));
    } else {
        echo "Fetch error";
    }
} else {
    $type = "loginFail";
    $failure = $twig->load('submitSuccess.html.twig');
    echo $failure->render(array("type"=>$type));
}


?>