<?php

require_once 'sqlhelper.php';
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$conn = connectToDatabase();
session_start();

if ($_SESSION["loggedIn"] == true) {
    if(isset($_GET["title"])) {
        $title = $_GET["title"];
        $article = $conn->query("call getArticle(\"$title\")");
        clearConnection($conn);
    }

    if(isset($_POST["article"])) {
        $articleTitle = $_POST["article"];
        $success = $conn->query("call addShare(\"$articleTitle\")");
        clearConnection($conn);

    } else if ($article) {
        $articleInfo = $article->fetch_all(MYSQLI_ASSOC);
        $articlePage = $twig->load('articlePage.html.twig');
        $log = $_SESSION["loggedIn"];
        echo $articlePage->render(array("article"=>$articleInfo, "login"=>"$log"));
    } else {
        echo "Fetch error";
    }
} else {
    $type = "loginFail";
    $failure = $twig->load('submitSuccess.html.twig');
    echo $failure->render(array("type"=>$type));
}


?>