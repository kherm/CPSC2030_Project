CREATE DATABASE dzungarian;

CREATE TABLE users (
    id text,
    pwd text
);

CREATE TABLE subscribers (
    email text
);

CREATE TABLE newsArticles (
    dates DATE,
    title text,
    author text,
    content text,
    photoId text,
    shares int
);

CREATE TABLE blogArticles (
    dates DATE,
    title text,
    author text,
    content text,
    photoId text,
    shares int
);

CREATE TABLE historyArticles (
    dates DATE,
    title text,
    author text,
    content text,
    photoId text,
    shares int

);

CREATE TABLE foodPhotos (
    restaurant text,
    meal text,
    photoId text
);

CREATE TABLE events (
    title text,
    dates text,
    photoId text,
    content text
);

CREATE TABLE userStories (
    fName text,
    lName text,
    email text,
    date DATE,
    content text
);


Insert into users values
    ('user1', 'user1'),
    ('user2', 'user2'),
    ('user3', 'user3');

Insert into foodPhotos values
    ("Chef Chu's", "Chili Curry", "XoByiBymX20"),
    ("Peaceful Coffee", "Healthy Salad", "OmRMR2yPuS4"),
    ("San Tung Eatery", "Seafood Stirfry", "Pt_YmiYm7a4"),
    ("Chef Chu's", "Garlic Mussels", "qvIrI4ueqzY"),
    ("Old Dzungarian Noodle House", "Dzungarian Noodle", "6RJct18G_3I"),
    ("Dzungarian Cafe", "Spicy Veggie Noodle Bowl", "zOlQ7lF-3vs"),
    ("San Tung Eatery", "Teriyaki Chicken", "4CJ9AWZT7FM"),
    ("Little Sheep Mongolian Hot Pot", "Pan Fry Goat and Rice", "uuaoIvWKOrg"),
    ("Little Sheep Mongolian Hot Pot", "Dzungarian Beef Stew", "xKSRpUH0VZo"),
    ("Chef Chu's", "Spicy Tuna and Salmon Combo", "PW8K-W-Kni0"),
    ("San Tung Eatery", "Pork Dumpling", "Dghn39rncfY"),
    ("Dzungarian Cafe", "Waffle Ice Cream", "MrmWoU9QDjs");

Insert into events values
    ("Fireworks!", "Aug. 7, 2018, 10pm", "87oQ_cUO1Ns", "Celebrating the start of the summer festival."),
    ("Live Music", "Aug. 10, 2018, 7pm", "2UuhMZEChdc", "Meet at the Dzungarian Cafe for some soulful tunes by artist Dan Mangan."),
    ("Food Festival", "Aug. 12-15, 2018", "mdWyghy08vg", "Discover traditional and other kinds of fun food. Happening on Main Street from 10am to 5pm."),
    ("Long Weekend", "Aug. 20, 2018", "C8jNJslQM3A", "Most stores and restaurants will be closed for the national holiday."),
    ("Live Music", "Aug. 25, 2018, 7pm", "Y20JJ_ddy9M", "Meet at the Dzungarian Cafe for K-pop inspired artist Kao."),
    ("Hiking Group", "Aug. 27, 2018, 8am", "UjxL9x0nwQ8", "Join the Dzungarian Hiking Club for a day hike along the Tien Shan ridge.");


Insert into newsArticles values
    ('2013-8-20', 'ad', 'Linda Morgan', 'Cupidatat ea nisi est fugiat do reprehenderit velit incididunt. Ut est eiusmod culpa occaecat qui sit dolore mollit eu. Commodo elit excepteur nulla ad dolore. Deserunt et nulla labore fugiat mollit. Pariatur id excepteur minim aliqua et laboris quis. Id est aliqua cupidatat dolor qui ullamco irure.', '-c2EzoCStOw', '5484'),
    ('2016-1-1', 'tempor Lorem esse in irure id', 'Carol Murphy', 'Deserunt voluptate cupidatat officia tempor duis officia dolor incididunt ipsum officia occaecat ea. Labore sint mollit culpa mollit irure et cillum cupidatat culpa reprehenderit dolore cillum. Elit adipisicing aliquip laborum aliqua ipsum exercitation esse. Culpa culpa ullamco sunt aute minim sint. Ex ad est ea non do sit sint velit aliqua incididunt.

    Adipisicing aliqua aliquip eu non proident ullamco nisi adipisicing aliqua commodo fugiat tempor pariatur sunt. Elit do duis commodo do occaecat laboris exercitation sit qui aliquip id sint. Cillum amet reprehenderit elit magna ipsum laboris adipisicing cupidatat. Sint sint deserunt fugiat quis laborum labore excepteur culpa. Incididunt dolore ipsum exercitation incididunt commodo aliquip amet. Dolor dolor irure ad aliquip culpa Lorem incididunt commodo labore exercitation fugiat aliqua duis. Anim ullamco amet aliquip ut.', '0HPej0iMHuY', '9983'),
    ('2010-1-28', 'est adipisicing pariatur deserunt eiusmod enim anim', 'Gloria Taylor', 'Fugiat Lorem adipisicing esse ea cupidatat velit id irure. Nostrud proident voluptate fugiat dolore id cupidatat. Fugiat aliqua consequat dolor dolor. Anim nisi enim magna duis aute do tempor amet culpa et id elit aliqua magna. Ipsum id sint non nulla proident. Occaecat cupidatat sint tempor eiusmod minim eiusmod consequat est ipsum ea minim quis.', 'bWg-BeVJPG4', '985'),
    ('2013-3-8', 'voluptate officia laborum est ex do', 'Jessica Harris', 'Incididunt velit sit dolore occaecat nostrud elit. Nostrud minim quis ex deserunt cupidatat proident in tempor duis labore. Ad ipsum incididunt anim ad aliqua mollit laboris.', 'qS1g9RIJx3k', '2213'),
    ('2010-12-8', 'fugiat', 'Linda Morgan', 'Nulla non occaecat esse est ullamco in ipsum velit. Ut esse Lorem veniam qui Lorem elit veniam. Eu ut et excepteur proident irure dolore laboris sint irure est et elit voluptate enim.

    Adipisicing sit Lorem deserunt dolore quis occaecat nisi. Est dolore labore ut elit tempor amet. Cillum esse elit commodo consequat. Mollit proident amet aliquip nulla nisi. Incididunt cupidatat dolor deserunt cillum irure id exercitation irure aliquip.', '16CAr7w5zXU', '4586'),
    ('2010-3-22', 'minim enim labore pariatur deserunt veniam', 'Alan Stewart', 'Enim est occaecat do veniam excepteur est qui deserunt aliqua et commodo id. Laboris elit cupidatat Lorem enim consequat. Ad id commodo non cupidatat cillum consectetur laboris aliquip cillum exercitation consectetur enim cillum. Labore quis qui nisi esse. Incididunt exercitation irure excepteur pariatur reprehenderit laborum sint cillum. Quis enim anim cillum dolore ullamco et exercitation.', 'qutA8URiA60', '2156'),
    ('2016-3-12', 'culpa in incididunt dolore', 'Joe Sanders', 'Aliqua ex fugiat enim occaecat qui veniam excepteur esse officia tempor sunt ad. Proident consequat laboris non ea pariatur nisi irure dolore ad elit. Adipisicing excepteur consectetur non id ullamco reprehenderit irure minim nostrud et duis ut exercitation sit.

    Pariatur nostrud ullamco sit proident aliqua sit. Pariatur qui anim id id nulla sint fugiat. Sunt reprehenderit pariatur duis Lorem labore culpa occaecat. Magna ut id nisi et officia minim.', 'X829kDjTcxo', '7943'),
    ('2010-6-24', 'veniam laborum elit mollit tempor', 'Jessica Harris', 'Proident proident ipsum do velit pariatur incididunt proident ex voluptate qui consequat. Ea ut eu est irure aliquip deserunt sunt aute officia. Nisi sunt cillum pariatur ea id esse ullamco laboris occaecat. Dolor anim occaecat aute ex aliqua culpa. Laborum irure dolor est amet officia et pariatur aliquip commodo esse id tempor. Ex deserunt incididunt commodo duis tempor aute nulla pariatur ad ullamco tempor velit sunt culpa.

    Veniam consequat pariatur in quis consectetur ipsum esse duis. Nulla eiusmod non aliqua cillum cillum minim ullamco. Cillum velit ipsum do dolore aliqua commodo nisi eu eiusmod ut ad ipsum aliquip adipisicing. Nulla reprehenderit eu non incididunt. Ad dolore excepteur voluptate velit minim occaecat magna dolore excepteur fugiat irure. Anim laborum culpa magna duis non et deserunt aute aliquip.', '-wWRHIUklxM', '4898'),
    ('2012-2-11', 'dolore eu nisi reprehenderit id', 'Carol Murphy', 'Esse incididunt irure aute laborum cillum et nostrud non voluptate. Magna occaecat culpa ullamco nisi enim do. Duis non ad aliquip sit duis officia dolor. Reprehenderit sit non ex tempor. Minim consectetur ad nisi eiusmod aliquip non voluptate quis veniam. Aliqua voluptate reprehenderit ipsum proident esse dolore est deserunt sint.

    Ullamco sint cupidatat occaecat duis et laboris id. Laborum enim non dolor ex Lorem laborum deserunt elit ea. Ad in eiusmod excepteur exercitation laboris.', 'WKT3TE5AQu0', '3578'),
    ('2018-5-25', 'exercitation duis', 'Gloria Taylor', 'Sint cupidatat sint labore sint quis officia culpa reprehenderit incididunt aute ipsum Lorem. Lorem tempor dolore laborum id aute dolore est laborum. Enim non quis fugiat deserunt. Minim pariatur officia mollit incididunt sunt pariatur non. Sit qui ullamco voluptate nulla velit veniam incididunt reprehenderit proident quis. Ex mollit ullamco culpa cupidatat nulla sit do anim ad do adipisicing.

    Ipsum laboris ipsum tempor aliqua voluptate anim officia cupidatat ut ipsum est. Eiusmod voluptate irure commodo commodo consectetur aliquip labore amet ea ex anim. In sunt nostrud ullamco occaecat magna velit sunt deserunt voluptate anim id magna et. Laborum adipisicing dolore dolor culpa voluptate nisi anim exercitation occaecat exercitation et.

    Minim cupidatat Lorem anim exercitation culpa reprehenderit voluptate cillum irure sit culpa aliquip qui. Et eiusmod enim nisi nulla non aliqua. Mollit amet laboris nostrud fugiat ex proident officia est. Qui sit eiusmod sint ea ullamco dolore dolore mollit elit deserunt commodo enim. Exercitation reprehenderit dolor ullamco sunt amet officia elit minim dolore officia quis pariatur eu. Anim id quis sunt eu aliqua velit consequat voluptate qui sit cillum incididunt fugiat voluptate.', 'OkzfLiNb40Y', '1511'),
    ('2018-5-10', 'velit velit ad ex', 'Lillian Clark', 'Duis eu adipisicing ullamco exercitation est ullamco. Sunt ea ea reprehenderit cillum et laborum duis incididunt consequat in magna occaecat. Occaecat eiusmod et quis minim deserunt cupidatat consectetur id eiusmod elit pariatur excepteur.', 'KhUCBmQKl-Q', '4083'),
    ('2016-10-19', 'nisi', 'Lillian Clark', 'In consectetur pariatur sint culpa. Commodo dolore sunt tempor exercitation mollit exercitation pariatur deserunt fugiat quis. Reprehenderit in cillum sint irure eiusmod.', 'L5mvKk4vi8A', '6699');


Insert into blogArticles values
    ('2015-5-27', 'minim aliqua officia velit', 'Nicholas Henderson', 'Consectetur commodo in velit eu id fugiat ex sint fugiat Lorem pariatur. Consequat ad ad pariatur proident eu id proident Lorem aute. Duis culpa culpa est voluptate et culpa nulla veniam aliquip dolore exercitation consectetur. Consequat aute labore enim proident cillum consectetur et aute fugiat quis commodo duis id.', '8hILOCKw5yA', '7395'),
    ('2013-3-4', 'voluptate est duis', 'Joe Sanders', 'Ullamco id irure laboris qui. Minim adipisicing sint incididunt aute. Eu incididunt ullamco minim pariatur aliqua mollit eu. Nostrud ut eiusmod officia est consequat minim commodo excepteur enim consequat sunt qui aute dolor. Commodo enim ut excepteur aliqua eiusmod duis. Laboris laborum ipsum deserunt culpa ullamco.

    Do excepteur non eu aliquip ut veniam eu. Eu eu do consectetur ea veniam magna mollit voluptate do ea consequat. Pariatur occaecat voluptate aliqua cillum reprehenderit laborum. Officia laborum nostrud adipisicing eiusmod id sit elit.', 'L0o1RfQuPUY', '4637'),
    ('2010-9-30', 'sunt nisi tempor', 'Carol Murphy', 'Cupidatat officia labore deserunt nulla irure. Labore adipisicing duis incididunt adipisicing ex ipsum cillum tempor culpa sunt magna reprehenderit ea sit. Sunt aliqua dolore esse consectetur cillum culpa nisi do cillum elit esse esse occaecat. Aute ex laborum cillum magna culpa magna ut sunt et ullamco laborum ex pariatur. Reprehenderit ut proident irure nisi ex ea voluptate voluptate et fugiat.', 'w01neVVNKHA', '3974'),
    ('2018-8-15', 'nostrud qui duis dolore dolor non', 'Lillian Clark', 'Culpa officia magna qui non anim nisi ad culpa commodo eu sit minim. Dolor irure ad id labore qui esse quis officia velit laborum. Velit est officia commodo minim sit esse ad nulla cupidatat anim aute. Anim amet consectetur velit mollit cupidatat culpa ex qui aliqua eiusmod ullamco. Laboris adipisicing duis ex anim culpa deserunt non deserunt excepteur labore. Nisi qui cupidatat occaecat labore.', 'Kf7aS1U1jBo', '4894'),
    ('2011-6-20', 'commodo anim sint Lorem duis', 'Russell Simmons', 'Nisi ex nulla sit exercitation sint dolore cupidatat sit voluptate aliqua magna excepteur. Mollit Lorem consequat ipsum aliquip nostrud consectetur officia adipisicing officia minim deserunt elit deserunt. Ullamco reprehenderit labore laboris cupidatat voluptate consequat enim. Dolor excepteur duis duis non laborum enim. Est cupidatat sunt ipsum culpa occaecat Lorem. Sit excepteur qui incididunt sint aliqua duis anim elit enim consequat sunt minim.', 'wT3mSWK5aqg', '7901'),
    ('2018-9-1', 'sunt irure nostrud laborum', 'Alan Stewart', 'Eu velit sit aliqua proident ex reprehenderit ullamco. Excepteur dolor do aliquip minim. Irure amet sint ad excepteur excepteur nulla aute ex exercitation id fugiat. Magna incididunt incididunt eiusmod ullamco tempor ex cupidatat consectetur aliquip eiusmod laborum ullamco. Pariatur et ex cillum minim ut excepteur cillum dolore sunt. Nulla id commodo laboris laboris sunt proident consequat quis id et. Pariatur sunt proident laboris enim duis duis duis duis.

    Irure dolor non labore eiusmod dolore. Incididunt eu duis ad aute excepteur sint. Velit dolore laborum laborum anim. Do enim sunt culpa pariatur labore duis do eiusmod nulla. Cupidatat nisi occaecat eiusmod incididunt nulla ea duis cillum exercitation anim nulla laborum culpa sint. Nisi veniam adipisicing nostrud occaecat.

    Amet dolore exercitation incididunt duis culpa laboris exercitation voluptate. Ex proident ut dolore ullamco culpa consectetur dolore ex consequat quis tempor eiusmod ex amet. Nulla in sit nostrud ullamco elit magna enim pariatur excepteur. Voluptate est pariatur ad mollit. Ullamco sint proident cillum dolore culpa velit excepteur proident.', 'vfaYObaMRew', '2966'),
    ('2011-3-5', 'sit', 'Carol Murphy', 'Veniam elit laboris cupidatat veniam elit. Dolor dolor non deserunt Lorem. Lorem eu exercitation eu nostrud minim cillum. Nisi et dolor labore sit excepteur aute elit aute magna mollit irure cillum nostrud. Consectetur culpa nulla anim laboris aute aliquip in aute velit aute adipisicing.

    Veniam do adipisicing excepteur magna irure et in magna consequat elit laboris aute ut. Veniam esse deserunt velit velit nisi cupidatat cupidatat Lorem magna reprehenderit ad ullamco aute. Sunt proident aliquip minim est excepteur amet esse sunt eu consequat id aliquip ullamco dolore. Eu aute veniam aliqua reprehenderit non reprehenderit labore consectetur fugiat velit nulla sit sit.

    Do quis qui sint amet consequat commodo commodo. Sint magna adipisicing mollit irure veniam mollit commodo consectetur cupidatat voluptate nulla voluptate ex in. Laborum minim irure est eiusmod duis sint aliqua quis laboris irure enim irure. Voluptate voluptate non ex esse exercitation qui laborum eiusmod minim aute deserunt minim irure consectetur. Proident cillum proident commodo sunt occaecat consequat mollit adipisicing ea.', 'YQPTf86GvpM', '2163'),
    ('2015-11-14', 'nulla est consectetur', 'Carol Murphy', 'Exercitation et reprehenderit non commodo magna esse. Culpa ex cupidatat nulla sint aliqua quis velit pariatur reprehenderit ipsum officia quis cupidatat. Reprehenderit in velit occaecat Lorem eu ullamco ea reprehenderit nisi. Excepteur labore aliqua sint nostrud. Velit ullamco minim ullamco aliquip enim.', 'DcNf9CgbXKQ', '3209'),
    ('2014-8-16', 'occaecat fugiat eu', 'Lillian Clark', 'Veniam adipisicing voluptate laborum qui ea consectetur. Lorem excepteur enim occaecat proident nostrud labore commodo aliqua id officia. Amet consequat aute cupidatat consequat officia nostrud eiusmod laboris duis id deserunt elit. Anim incididunt mollit proident officia eiusmod excepteur laboris ullamco sit eu sint. Anim excepteur nisi deserunt exercitation exercitation nulla consectetur labore. Sit Lorem fugiat in sit sunt in ea veniam. Cillum anim sit in ad non quis irure irure.

    Exercitation fugiat do non ut do ad dolor anim ut. Ea aliquip adipisicing velit cupidatat sunt reprehenderit adipisicing proident ipsum incididunt anim. Quis ad eu consectetur incididunt sunt anim aute mollit enim incididunt laboris commodo.', 'p9vr45T2scg', '752'),
    ('2018-7-14', 'excepteur ad', 'Carol Murphy', 'Et sint proident veniam eu fugiat nisi officia. Eu adipisicing amet laboris est ex tempor Lorem Lorem incididunt mollit qui incididunt elit. Eiusmod nulla est veniam incididunt officia ipsum aute nulla non aliqua nisi in sint deserunt. Ex ex officia pariatur exercitation esse mollit fugiat labore aliquip fugiat dolor magna quis.

    Deserunt est id ullamco Lorem eiusmod esse fugiat ad amet. Esse consectetur culpa nisi dolore aute reprehenderit in cillum anim commodo Lorem reprehenderit est. Occaecat esse esse nostrud amet ea qui ullamco aute ex enim ad consequat minim.', 'HirT7xlIBMg', '3773');


Insert into historyArticles values
    ('2015-11-18', 'ipsum officia commodo ut', 'Gloria Taylor', 'Sunt duis velit Lorem minim enim ex sit laborum ut deserunt. Exercitation tempor sit do occaecat culpa. Ex occaecat sint aliquip non excepteur mollit consectetur.', 'u4hXJLPr3Kw', '4506'),
    ('2010-1-17', 'deserunt duis voluptate esse aliqua ullamco', 'Alan Stewart', 'Adipisicing dolore duis deserunt velit deserunt mollit laboris aute. Nisi deserunt ea irure commodo proident fugiat. Sit occaecat fugiat sit ipsum sunt.

    Aliqua culpa elit labore officia nostrud exercitation cupidatat velit sint et dolor consequat laboris aliqua. Esse consectetur laboris cupidatat exercitation in culpa excepteur adipisicing qui ex dolor voluptate. Veniam ut sunt laborum mollit tempor tempor minim Lorem duis ullamco. Excepteur minim officia ea non ullamco non quis. Nulla consequat eiusmod id velit adipisicing reprehenderit duis veniam consectetur aliquip labore.', '0BSFfSA08pA', '1028'),
    ('2013-5-6', 'ipsum qui eu consequat exercitation irure consectetur', 'Louise Brooks', 'Amet eiusmod aliquip non nulla laborum amet sint fugiat est consequat cupidatat in consectetur sunt. Enim velit sint consectetur eiusmod ex. Incididunt ad ullamco laboris aliquip officia tempor do. Officia esse qui ad duis in pariatur nisi eu. Laboris eu laborum voluptate eu ea excepteur in elit.

    Nulla fugiat occaecat consectetur aliquip officia pariatur aliquip sit ipsum officia. Occaecat laboris tempor et cupidatat non Lorem ut voluptate cillum. Exercitation do esse sit tempor officia deserunt ipsum quis culpa mollit consequat sunt laborum. Deserunt non Lorem mollit aliqua consectetur aute incididunt enim reprehenderit cillum. Irure eu nulla est pariatur ullamco elit labore labore.', 'PTw4YRmQCsI', '8623'),
    ('2010-3-10', 'ea occaecat duis velit elit laboris magna', 'Linda Morgan', 'Elit nostrud consequat laboris id quis culpa. Do nulla id aute laborum incididunt culpa consectetur laboris cupidatat. Minim sit irure consectetur nulla enim. Veniam commodo aute amet sit ipsum quis deserunt consequat consectetur adipisicing nostrud magna adipisicing. Minim exercitation officia veniam laborum ea quis est incididunt Lorem elit duis occaecat. Sit anim incididunt cupidatat tempor nostrud commodo qui reprehenderit ut reprehenderit ea ad in nulla.', '2Z8IJdJGP-E', '6154'),
    ('2017-11-7', 'culpa id', 'Carol Murphy', 'Commodo amet ut minim aliquip reprehenderit dolor. Labore ut elit veniam cupidatat. Voluptate commodo voluptate id cillum sunt culpa. Officia ex consectetur nulla anim voluptate et nisi minim pariatur proident velit. Ut commodo officia ad aliquip nostrud anim.

    Reprehenderit ipsum minim reprehenderit aliqua exercitation proident. Nisi non occaecat consequat officia est non id enim consequat sit non. Et fugiat labore irure veniam non irure et ut amet excepteur deserunt cillum dolor nisi.', 'd6m2xV4ekfI', '9910');