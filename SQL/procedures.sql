DELIMITER //

CREATE OR REPLACE PROCEDURE ckLogin(IN loginid text, IN loginpwd text)
BEGIN
    SELECT * FROM users WHERE id = loginid AND pwd = loginpwd;
END //


CREATE OR REPLACE PROCEDURE addUser(IN loginid text, IN loginpwd text)
BEGIN
    INSERT INTO users VALUES (loginid, loginpwd);
END //


CREATE OR REPLACE PROCEDURE getArticlesFor (IN tableName text)
BEGIN
    SET @table = tableName;
    SET @stmnt = CONCAT('SELECT dates, title, photoId, LEFT(content,200) AS highlight FROM ', @table, ';');
    PREPARE statement from @stmnt;
    EXECUTE statement;
    DEALLOCATE PREPARE statement;
END //


CREATE OR REPLACE PROCEDURE getFoodPhotos ()
BEGIN
    SELECT * FROM foodphotos;
END //


CREATE OR REPLACE PROCEDURE getEvents ()
BEGIN
    SELECT * FROM events;
END //


CREATE OR REPLACE PROCEDURE getNumNewestArticles (IN num int)
BEGIN
    SELECT dates, title, photoId, LEFT(content, 200) AS highlight FROM (
        SELECT * FROM blogArticles
        UNION
        SELECT * FROM newsArticles
    ) as allArticles
    ORDER BY dates DESC LIMIT num;
END //


CREATE OR REPLACE PROCEDURE getArticle (IN articleTitle text)
BEGIN
    SELECT * FROM
    ( SELECT * FROM blogArticles UNION SELECT * FROM newsArticles UNION SELECT * FROM historyArticles ) as allArticles
    WHERE title = articleTitle;
END //


CREATE OR REPLACE PROCEDURE addSubscriber (IN eAddress text)
BEGIN
    INSERT INTO subscribers VALUES (eAddress);
END //


CREATE OR REPLACE PROCEDURE addUserStory (IN fName text, IN lName text, IN email text, IN dates date,IN content text)
BEGIN
    INSERT INTO userStories VALUES (fName, lName, email, dates, content);
END //


CREATE OR REPLACE PROCEDURE addShare (IN articleTitle text)
BEGIN
    UPDATE blogArticles SET shares = shares + 1 WHERE title = articleTitle;
    UPDATE newsArticles SET shares = shares + 1 WHERE title = articleTitle;
    UPDATE historyArticles SET shares = shares + 1 WHERE title = articleTitle;
END //

DELIMITER ;


