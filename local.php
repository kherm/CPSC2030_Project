<?php

require_once 'sqlhelper.php';
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);
$conn = connectToDatabase();
session_start();


if ($_SESSION["loggedIn"] == true) {
    if(isset($_GET["page"])) {
        $page = $_GET["page"];

        if($page == "Cool Eats") {
            $food = $conn->query("call getfoodPhotos()");
            clearConnection($conn);
            if($food) {
                $foodInfo = $food->fetch_all(MYSQLI_ASSOC);
                $pageContent = $twig->load('coolEats.html.twig');
                echo $pageContent->render(array("foodPhotos"=>$foodInfo));
            }
        }

        else if($page == "Upcoming Events") {
            $events = $conn->query("call getEvents()");
            clearConnection($conn);
            if($events) {
                $eventInfo = $events->fetch_all(MYSQLI_ASSOC);
                $pageContent = $twig->load('upcomingEvents.html.twig');
                echo $pageContent->render(array("eventInfo"=>$eventInfo));
            }
        }


    } else {
        $success = $twig->load('localPage.html.twig');
        $log = $_SESSION["loggedIn"];
        echo $success->render(array("login"=>"$log"));
    }

} else {
    $type = "loginFail";
    $failure = $twig->load('submitSuccess.html.twig');
    echo $failure->render(array("type"=>$type));
}




?>